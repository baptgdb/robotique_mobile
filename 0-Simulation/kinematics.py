from math import cos, sin, acos, pi, sqrt, atan2, fmod
import constants as c
# computeDK function n arguments

def computeDK(*args):
    """
    la fonction computeDK calcule les coordonnées de P3 en fonction des angles des moteurs.
    
    args: 3 arguments
        args[0]: angle du moteur 1
        args[1]: angle du moteur 2
        args[2]: angle du moteur 3
        
    return: 3 coordonnées de P3
        p3x: coordonnée x de P3
        p3y: coordonnée y de P3
        p3z: coordonnée z de P3
        
    computeDK utilise les constantes de la patte du robot, L1, L2 et L3,
    mais aussi les fonctions cos et sin de la bibliothèque math.
    """
    
    # constantes de la patte du robot
    L1 = c.constL1
    L2 = c.constL2
    L3 = c.constL3
    
    # angles des moteurs
    θ1 = args[0]
    θ2 = args[1]
    θ3 = args[2]
    
    if len(args) < 3 or len(args) > 3:
        print ("ERROR: computeDK function should takes 3 arguments\n :(  ")
        return None;        # dans le cas où on a pas 3 arguments on ne peut pas calculer les angles.

    P1 = [L1*cos(θ1),L1*sin(θ1),0] # coordonnées de P1

    L2proj = L2*cos(θ2) # projection de L2 sur le plan xOy
    
    # coordonnées de P2
    p2x = (L1+L2proj)*cos(θ1)
    p2y = (L1+L2proj)*sin(θ1) 
    p2z = L2*sin(θ2)

    P2 = [p2x,p2y,p2z] # coordonnées de P2

    α = θ2+θ3 # angle entre L2 et L3
    L3proj = L3*cos(α) # projection de L3 sur le plan xOy
    Lproj = L1+L2proj+L3proj # projection de L3 sur le plan xOy
    
    # coordonnées de P3
    p3x = Lproj*cos(θ1) 
    p3y = Lproj*sin(θ1)
    p3z = p2z+L3*sin(α)

    return [p3x,p3y,-p3z] # on retourne les coordonnées de P3

def computeIK(*args):
    """
    fonction qui calcule les angles des moteurs en fonction des coordonnées de P3
    
    args: 3 arguments
        args[0]: coordonnée x de P3
        args[1]: coordonnée y de P3
        args[2]: coordonnée z de P3
    
    return: 3 angles des moteurs
        θ1: angle du moteur 1
        θ2: angle du moteur 2
        θ3: angle du moteur 3
    
    computeIK calcule les angles en utilisant Al-Kashi et pythagore.
    
    computeIK utilise les constantes de la patte du robot, L1, L2 et L3.
    mais aussi les fonctions atan2, acos, et sqrt de la bibliothèque math.
    la fonction alkashi est une fonction locale à computeIK qui permet de calculer des angles grace a Al-Kashi.
    """
    
    # constantes de la patte du robot
    L1 = c.constL1
    L2 = c.constL2
    L3 = c.constL3
    
    # coordonnées de P3
    p3x = args[0]
    p3y = args[1]
    p3z = args[2]
    
    if len(args) < 3 or len(args) > 3:
        print ("ERROR: computeDK function should takes 3 arguments\n :(  ")
        return None;        # dans le cas où on a pas 3 arguments on ne peut pas calculer les angles.

    θ1 =  atan2(p3y,p3x) 
    Lproj = sqrt(p3x**2 + p3y**2)   # distance entre l'origine et P3proj
    d13 = Lproj - L1                # distance entre P1 et P3
    d = sqrt(p3z**2 + d13**2)       # distance entre P3 et P2
    a = atan2(p3z,d13)              # angle entre d et d13

    def alkashi(a, b, c):       # calcule l'angle entre a et b connaissant les longueurs des 3 cotés du triangle
        return acos(max(-1, min(1, (a**2 + b**2 - c**2)/(2*(a*b)))))

    b = alkashi(L2, d, L3)              # angle entre L2 et d
    θ2 = pi/2 if p3z == L2 else a + b   # angle entre L2 et d13

    if d > L2+L3 or d < abs(L2-L3):     # si d est trop grand ou trop petit
        θ3 = 0                          # alors on ne peut pas atteindre les coordonnées demandées le bras est tendu au maximum
        
    θ3 = alkashi(L2, L3, d) + pi        # angle entre L2^L3 
    return [θ1,-θ2,-θ3]


def circle(x, z, r, t, duration):
    """
    # x = p.readUserDebugParameter(sliders["circle_x"]) soit la coordonnée x du centre du cercle sur un plan tangent a la patte
    # z = p.readUserDebugParameter(sliders["circle_z"]) soit la coordonnée z du centre du cercle sur un plan tangent a la patte
    # r = p.readUserDebugParameter(sliders["circle_r"]) soit le rayon du cercle
    # t = sim.t soit le temps de la simulation 
    # duration = p.readUserDebugParameter(sliders["circle_duration"]) soit la durée du mouvement
    """
    
    if duration == 0:
        return computeIK(x, y, z)

    
    y = r * cos(2 * pi * t / duration)
    z += r * sin(2 * pi * t / duration)
    return computeIK(x, y, z)


def triangle(x, z, h, w, t):
    """
    # x = p.readUserDebugParameter(sliders["triangle_x"]) soit la coordonnée x du centre du triangle sur un plan tangent a la patte
    # z = p.readUserDebugParameter(sliders["triangle_z"]) soit la coordonnée z du centre du triangle sur un plan tangent a la patte
    # h = p.readUserDebugParameter(sliders["triangle_h"]) soit la hauteur du triangle
    # w = p.readUserDebugParameter(sliders["triangle_w"]) soit la largeur du triangle
    # t =  sim.t soit le temps de la simulation
    """
    
    if w == 0:
        return computeIK(x, 0, z + h)
    
    if h == 0:
        return computeIK(x, w/2, z)
    
    points = [
        [x, 0, z + h],
        [x, w/2, z],
        [x, -w/2, z]
    ]

    point1 = points[int(t) % 3]
    point2 = points[(int(t) + 1) % 3]

    T = t % 1

    M = [
        point1[0] + T * (point2[0] - point1[0]),
        point1[1] + T * (point2[1] - point1[1]),
        point1[2] + T * (point2[2] - point1[2])
    ]

    return computeIK(M[0], M[1], M[2])


def segment(segment_x1, segment_y1, segment_z1, segment_x2, segment_y2, segment_z2, t, duration):
    """
    # segment_x1 = p.readUserDebugParameter(sliders["segment_x1"]) soit la coordonnée x du premier point du segment 
    # segment_y1 = p.readUserDebugParameter(sliders["segment_y1"]) soit la coordonnée y du premier point du segment 
    # segment_z1 = p.readUserDebugParameter(sliders["segment_z1"]) soit la coordonnée z du premier point du segment 
    # segment_x2 = p.readUserDebugParameter(sliders["segment_x2"]) soit la coordonnée x du deuxième point du segment 
    # segment_y2 = p.readUserDebugParameter(sliders["segment_y2"]) soit la coordonnée y du deuxième point du segment 
    # segment_z2 = p.readUserDebugParameter(sliders["segment_z2"]) soit la coordonnée z du deuxième point du segment 
    # t = sim.t soit le temps de la simulation
    # duration = p.readUserDebugParameter(sliders["segment_duration"]) soit la durée du mouvement
    """
        
    points = [
        [segment_x1, segment_y1, segment_z1],
        [segment_x2, segment_y2, segment_z2]
    ]

    point1 = points[int(t/duration) % 2]
    point2 = points[(int(t/duration) + 1) % 2]

    T = (t % duration) / duration

    M = [
        point1[0] + T * (point2[0] - point1[0]),
        point1[1] + T * (point2[1] - point1[1]),
        point1[2] + T * (point2[2] - point1[2])
    ]
    
    return computeIK(M[0], M[1], M[2])