#!/usr/bin/python
# -*- coding: utf-8 -*-

import serial
from time import sleep


# https://emanual.robotis.com/docs/en/dxl/protocol1/
    # http://support.robotis.com/en/product/actuator/dynamixel/ax_series/dxl_ax_actuator.htm

# https://www.rapidtables.com/convert/number/hex-to-decimal.html


class Dynamixel_AX_12a():
    # port = Bus 001 Device 005: ID 16d0:06a7 MCS USB2AX
    # port : /dev/ttyACM0
    def __init__(self, port, baud, timeout, id):
        self.id = id
        self.ser = serial.Serial(port=port, baudrate=baud, timeout=timeout)
        if self.ser.isOpen():
            print("serial.Serial OK")
        else:
            print("serial.Serial ERROR")
            


    def close(self, ser):
        self.ser.close()


    def write_data(self, ser, data):
        self.ser.write(data)


    def read_data(self, ser, size=1):
        return self.ser.read(size)


    def to_hex(self, val):
        return chr(val)


    def decode_data(self, data):
        return "".join(f"{hex(ord(d))} " for d in data)


    def checksum(self,data):
        #return (~data) & 0xff
        value = sum(data)
        return (~value) & 0xFF
      
      
    def scan(self):
        try:
            for ID in range(254):
                print("ID : ", ID)

                # uart ping:
                cksm = 0xFF - (ID + 0x02 + 0x01)
                data = [0xFF, 0xFF, ID, 0x02, 0x01, cksm]

                # Call the write method on the Serial instance
                self.ser.write(data)
                print(data)
                a = self.ser.read(6)
                print(a)
                if len(a) == 6:
                    print("OK")
                    return ID 
        except Exception:
            print("Error")
            
        
        
    def Oscillation(self, speed):
        Led = 25
        
        for _ in range(5):
            # Header1 	Header2 	Packet ID 	Length 	Instruction 	Param 1 	… 	Param N 	Checksum
            data = [0xFF, 0xFF, self.id, 0x04, 0x03, Led, 1]
            data.append(self.checksum(data[2:]))
            self.ser.write(bytes(data))
            sleep(1)
            print(1)
            # Header1 	Header2 	Packet ID 	Length 	Instruction 	Param 1 	… 	Param N 	Checksum
            data = [0xFF, 0xFF, self.id, 0x04, 0x03, Led, 0]
            data.append(self.checksum(data[2:]))
            self.ser.write(bytes(data))
            sleep(1)
            print(2)


    def SetPosition(self, angle):
        # Calculate GoalPosition based on the available position range and per unit value
        GoalPosition = int(angle / 0.29296875)

        # Extract the lowest and highest bytes of GoalPosition
        GoalPosition_Low = GoalPosition & 0xFF
        GoalPosition_High = (GoalPosition>>8)&0xFF


        # Construct the command packet
        # Header1 	Header2 	Packet ID 	Length 	Instruction 	Param 1 	… 	Param N 	Checksum
        data = [0xFF, 0xFF, self.id, 0x05, 0x03, 0x1E, GoalPosition_Low, GoalPosition_High]

        GoalPosition = min(GoalPosition, 1023)
        data.append(self.checksum(data[2:]))
        print(data)

        # Send the command via the serial port
        self.ser.write(bytes(data))
        print("SetPosition OK")
        sleep(0.1)

    
    def LirePosition(self):
        # GoalPosition = 30
        
        # Header1  Header2  Packet ID  Length  Instruction  Param 1  …  Param N  Checksum
        data = [0xFF, 0xFF, self.id, 0x04, 0x02, 0x24, 0x02]
        
        checksum = self.checksum(data[2:])  # Calculate the checksum
        data.append(checksum)  # Add the checksum to the packet
        self.ser.write(data)  # Send the packet
        value = self.ser.read(size=8)  # Clear the serial port buffer
        print(f"value {value}")
        bit_to_int = int.from_bytes(value[5:7])
        print(f"decode_data {bit_to_int}")
        return 0




if __name__ == "__main__":
    
    test = Dynamixel_AX_12a("/dev/ttyACM0", 1000000, timeout=0.1, id=51)
    try:
        #test.scan()
        #sleep(1)
        #test.Oscillation(2)
        #sleep(1)
        test.SetPosition(150)
        print(test.LirePosition())
        
        # for i in range(0, 300, 2):
        #     print(f"Position: {i}")
        #     sleep(2)
        #     test.SetPosition(i)
        #     sleep(0.1)
        #     position_value = test.LirePosition()
        #     print(f"Set Position: {i}, Read Position: {position_value}\n")
            
        #     if i >= 300:
        #         print(f"Reached the maximum position limit ({i}). Exiting the loop.")
        #         break
    except Exception as e:
        print(f"Error opening serial port: {e}")